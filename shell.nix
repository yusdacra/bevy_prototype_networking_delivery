{ pkgs ? import <nixpkgs> { } }:
pkgs.mkShell {
  name = "bevy-dev-shell";
  nativeBuildInputs = with pkgs; [ pkg-config clang_10 lld_10 ];
  buildInputs = with pkgs; [ alsaLib x11 ];
}
