## bevy_prototype_networking_delivery
This is a simple abstraction over `bevy_prototype_networking_laminar`. It handles data (de)serialization (`bincode`) and compression (currently `lz4`) for you.

## Examples
Run `cargo run --package client` for the client. `cargo run --package server` for the server.