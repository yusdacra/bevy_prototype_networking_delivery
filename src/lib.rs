use bevy::prelude::*;
use bevy_prototype_networking_laminar::{Connection, NetworkResource, NetworkingPlugin};
use bytes::Bytes;
use recv::{poll_low_level_network_events, LowLevelNetworkEventReader};
use send::{deliver, SendEvent};
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use std::net::SocketAddr;

mod recv;
mod send;

pub use bincode::ErrorKind as ParseError;
pub use recv::{NetworkEvent, NetworkEvents};
pub use send::SendEvents;
pub use std::io::Error as IOError;
pub use std::net::AddrParseError;

/// A plugin which sets up a simple and easy-to-use networking system.
/// Handles compression and (de)serialization for you.
pub struct NetworkPlugin;

impl Plugin for NetworkPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_plugin(NetworkingPlugin)
            .add_event::<NetworkEvent>()
            .add_event::<SendEvent>()
            .init_resource::<NetworkConnectionInfo>()
            .init_resource::<LowLevelNetworkEventReader>()
            .init_resource::<NetworkManager>()
            .add_system(control_networking.system())
            .add_system(poll_low_level_network_events.system())
            .add_system(deliver.system());
    }
}

/// Information which the plugin needs for connecting to the network.
#[derive(Default, Debug, Copy, Clone)]
pub struct NetworkConnectionInfo {
    bind_addr: Option<SocketAddr>,
    server_addr: Option<SocketAddr>,
}

impl NetworkConnectionInfo {
    /// Sets the bind address and the server address to the given addresses (respectively).
    /// This means that the plugin will act as a client (only send `Delivery`s to `server_addr`)
    pub fn client(&mut self, addr: &str, server_addr: &str) -> Result<(), AddrParseError> {
        self.bind_addr = Some(addr.parse()?);
        self.server_addr = Some(server_addr.parse()?);
        Ok(())
    }

    /// Sets the bind adress to the given address, the server address is set to `None`.
    /// This means that the plugin will act as a server. (`Delivery`s are broadcasted by default, to all connections)
    pub fn server(&mut self, addr: &str) -> Result<(), AddrParseError> {
        self.bind_addr = Some(addr.parse()?);
        self.server_addr = None;
        Ok(())
    }

    /// Clears the bind address and the server address.
    /// This means that the plugin will unbind the next time the control system runs.
    pub fn clear(&mut self) {
        self.bind_addr = None;
        self.server_addr = None;
    }

    /// Checks if the contained information represents a server.
    /// If yes, returns `true`.
    pub fn is_server(&self) -> bool {
        self.server_addr.is_none()
    }

    /// Checks if the contained information represents a client.
    /// If yes, returns `true`.
    pub fn is_client(&self) -> bool {
        self.server_addr.is_some()
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct Payload {
    data: Bytes,
    is_reliable: bool,
    is_compressed: bool,
}

impl Payload {
    fn new(data: Bytes) -> Self {
        Self {
            data,
            is_reliable: false,
            is_compressed: false,
        }
    }
}

#[derive(Debug)]
enum DeliveryAddress {
    To(SocketAddr),
    From(SocketAddr),
}

/// Contains the payload to send and may contain an address if it will be sent to or has come from an address.
#[derive(Debug)]
pub struct Delivery {
    addr: Option<DeliveryAddress>,
    payload: Payload,
}

impl Into<SendEvent> for Delivery {
    fn into(self) -> SendEvent {
        SendEvent::new(self)
    }
}

impl Delivery {
    /// Creates a new `Delivery` using the given serializable type.
    ///
    /// # Errors
    /// Returns a `ParseError` if an error has occured while serializing the data.
    pub fn new(data: &impl Serialize) -> Result<Self, ParseError> {
        Ok(Self {
            addr: None,
            payload: Payload::new(bincode::serialize(data).map_err(|e| *e)?.into()),
        })
    }

    /// Creates a new `Delivery` using the given serializable type and sets the delivery adress to the given address.
    /// The set address is ignored if the plugin is configured to act as a client.
    ///
    /// # Errors
    /// Returns a `ParseError` if an error has occured while serializing the data.
    pub fn send_to(data: &impl Serialize, addr: SocketAddr) -> Result<Self, ParseError> {
        Ok(Self {
            addr: Some(DeliveryAddress::To(addr)),
            payload: Payload::new(bincode::serialize(data).map_err(|e| *e)?.into()),
        })
    }

    pub(crate) fn from(payload: Payload, addr: SocketAddr) -> Self {
        Self {
            addr: Some(DeliveryAddress::From(addr)),
            payload,
        }
    }

    /// Marks the payload to be transmitted reliably.
    pub fn reliable(mut self) -> Self {
        self.payload.is_reliable = true;
        self
    }

    /// Compresses the payload data in place.
    /// Data won't be compressed again if already compressed once.
    ///
    /// # Errors
    /// Returns an `IOError` if an error has occured while compressing the data.
    pub fn compress(mut self) -> Result<Self, IOError> {
        if self.payload.is_compressed {
            return Ok(self);
        }

        self.payload.data = lz4::block::compress(
            &self.payload.data,
            Some(lz4::block::CompressionMode::HIGHCOMPRESSION(21)), // 2 MB memory usage
            true,
        )?
        .into();

        self.payload.is_compressed = true;

        Ok(self)
    }

    /// Compresses the payload data, compares it with the uncompressed size and return the one that is smaller in size.
    /// Data won't be compressed again if already compressed once.
    ///
    /// # Errors
    /// Returns an `IOError` if an error has occured while compressing the data.
    pub fn compress_if_worth(self) -> Result<Self, IOError> {
        let before_data = self.payload.data.clone();
        let mut compressed = self.compress()?;

        if compressed.payload.data.len() >= before_data.len() {
            compressed.payload.data = before_data;
            compressed.payload.is_compressed = false;
        }

        Ok(compressed)
    }

    /// Returns the (uncompressed if it was compressed) payload data.
    ///
    /// # Errors
    /// Returns an `IOError` if an error has occured while decompressing the data.
    pub fn data(self) -> Result<Bytes, IOError> {
        Ok(if self.payload.is_compressed {
            lz4::block::decompress(&self.payload.data, None)?.into()
        } else {
            self.payload.data
        })
    }

    /// Deserializes the payload data to the given `T` type.
    ///
    /// # Errors
    /// Returns a `ParseError` if an error has occured while deserializing the data.
    /// May return `ParseError::Io(error)` variant if an IO error has occured while decompressing the data.
    pub fn deser<T: DeserializeOwned>(self) -> Result<T, ParseError> {
        let data = self.data().map_err(ParseError::Io)?;
        bincode::deserialize(&data).map_err(|e| *e)
    }
}

/// Event that is produced when a connection is established or lost.
#[derive(Debug)]
pub enum ConnectionEvent {
    /// Produced when a connection is lost.
    Disconnected(Connection),
    /// Produced when a connection is established.
    Connected(Connection),
}

#[derive(Default)]
struct NetworkManager {
    is_bound: bool,
}

fn control_networking(
    con_info: Res<NetworkConnectionInfo>,
    mut net_res: ResMut<NetworkResource>,
    mut net: ResMut<NetworkManager>,
) {
    if let Some(addr) = con_info.bind_addr {
        if !net.is_bound {
            if let Err(err) = net_res.bind(addr) {
                eprintln!("Could not bind to socket: {}", err);
                return;
            };
            net.is_bound = true;
        }
    } else if net.is_bound {
        // FIXME: Handle socket unbinding here
        net.is_bound = false;
    }
}
