use super::{ConnectionEvent, Delivery, ParseError};
use bevy::prelude::*;
use bevy_prototype_networking_laminar::{NetworkError, NetworkEvent as LowLevelNetworkingEvent};

#[derive(Default)]
pub(crate) struct LowLevelNetworkEventReader {
    network_events: EventReader<LowLevelNetworkingEvent>,
}

/// Event produced whenever something happens on the network.
#[derive(Debug)]
pub enum NetworkEvent {
    /// Produced when a `Delivery` is received.
    Received(Delivery),
    /// Produced when a payload is received, but can't be deserialized.
    MessageParseError(ParseError),
    /// Produced when an error occurs on the low level networking layer.
    NetworkError(NetworkError),
    /// Produced when a connection is established / lost.
    Connection(ConnectionEvent),
}
/// Convenience type to get `NetworkEvent`s.
pub type NetworkEvents = Events<NetworkEvent>;

pub(crate) fn poll_low_level_network_events(
    lownet_events: Res<Events<LowLevelNetworkingEvent>>,
    mut lownet_event_state: ResMut<LowLevelNetworkEventReader>,
    mut network_events: ResMut<Events<NetworkEvent>>,
) {
    for ev in lownet_event_state.network_events.iter(&lownet_events) {
        match ev {
            LowLevelNetworkingEvent::Message(con, data) => {
                network_events.send(NetworkEvent::Received(Delivery::from(
                    match bincode::deserialize(&data) {
                        Err(err) => {
                            network_events.send(NetworkEvent::MessageParseError(*err));
                            continue;
                        }
                        Ok(p) => p,
                    },
                    con.addr,
                )))
            }
            LowLevelNetworkingEvent::Disconnected(con) => {
                network_events.send(NetworkEvent::Connection(ConnectionEvent::Disconnected(
                    *con,
                )));
            }
            LowLevelNetworkingEvent::Connected(con) => {
                network_events.send(NetworkEvent::Connection(ConnectionEvent::Connected(*con)));
            }
            // FIXME: Handle errors?
            LowLevelNetworkingEvent::SendError(err) => eprintln!("{}", err),
        }
    }
}
