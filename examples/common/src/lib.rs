use bevy::math::Vec2;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub enum NetworkTypes {
    TestType { pos: Vec2, rot: f64 },
    Message(String),
}
